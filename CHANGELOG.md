# Aurora Antilles Francaises Changelog #

## Update 02 February 2025 ##
**{+ AIRAC : 2501 +}**

### Updated Files ###

- **FIXES files (fix)** : French_Antilles
- **VOR files (vor)** : French_Antilles
- **NDB files (ndb)** : French_Antilles
- **AWY files (hawy/lawy)** : French_Antilles
- **APT files (apt)** : French_Antilles
- **SID files (sid)** : TFFR
- **GND files (gnd)** : TFFF
- **Restrict files (restrict)** : French_Antilles
- **Taxiway label files (txi)** : TFFF
- **Gates label files (gts)** : TFFF
- **Colorscheme (clr)** : FRANCE

---
### Added Files ###

- **Symbols file (sym)** : FR
- **GND files (gnd)** : TFH2, TFH3

---
---
## Update 20 June 2024 ##
**{+ AIRAC : 2406 +}**

### Updated Files ###

- **FIXES files (fix)** : French_Antilles
- **VOR files (vor)** : French_Antilles
- **NDB files (ndb)** : French_Antilles
- **AWY files (hawy/lawy)** : French_Antilles
- **SID files (sid)** : TFFF
- **STAR files (str)** : TFFF

---
---
## Update 22 February 2024 ##
**{+ AIRAC : 2402 +}**

### Updated Files ###

- **FIXES files (fix)** : French_Antilles
- **AWY files (hawy/lawy)** : French_Antilles

---
### Deleted Files ###

- **STAR files (str)** : TFFM

---
---
## Update 30 November 2023 ##
**{+ AIRAC : 2312 +}**

### Updated Files ###

- **FIXES files (fix)** : French_Antilles
- **VOR files (vor)** : French_Antilles
- **NDB files (ndb)** : French_Antilles
- **AWY files (hawy/lawy)** : French_Antilles
- **MSRA files (mva)** : TFFF, TFFR
- **CCR files (artcc)** : TJZS, TTZP, SVZM
- **TMA files (hartcc)** : TFFF, TFFR
- **CTR files (lartcc)** : TFFF, TFFR
- **ATC File (atc)** : AFIS, TFFF, TFFR
- **SID files (sid)** : TFFF, TFFR
- **STAR files (str)** : TFFF, TFFR, TFFM<sup>X</sup>
- **VFR fixes files (vfi)** : TFFJ
- **GND files (gnd)** : TFFF, TFFR
- **Taxiway label files (txi)** : TFFF, TFFR
- **Gates label files (gts)** : TFFF, TFFR

---
### Added Files ###

- **ATIS file (atis)** : France
- **TMA files (hartcc)** : Other (TNCM, TAPA)
- **CTR files (lartcc)** : Other (TNCM, TQPF, TAPA, TKPK)

<sup>X</sup> : File content has been withdrawn and file will be deleted in the next update.

---
---
## Update 31 January 2021 ##
**{+ AIRAC : 2101 +}**

**No Applicable Updates to date**

---
---
## Update 17 November 2020 ##
**{+ AIRAC : 2012 +}**

**No Applicable Updates to date**

---
---
## Update 10 October 2020 ##
**{+AIRAC : 2011 +}**

### Added Files ###

- **VFR Fixes file (vfi)** : TFFF, TFFR

